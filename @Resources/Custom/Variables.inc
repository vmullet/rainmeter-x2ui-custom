[Variables]

;Animation effect images
ImageAnimFactor=1.25

; default = 1
ZoomFactor=1.80

;Cyclotrons
CyclotronWidth=75
CyclotronHeight=560

;Translations
DaysTranslationFR="Monday":"Lundi","Tuesday":"Mardi","Wednesday":"Mercredi","Thursday":"Jeudi","Friday":"Vendredi","Saturday":"Samedi","Sunday":"Dimanche"

;Colors
PrimaryColor=130,209,222
SecondaryColor=235,249,188
PrimaryLineColor=130,209,222,127


